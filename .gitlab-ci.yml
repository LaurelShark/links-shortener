workflow:
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_BRANCH == 'main'

variables:
  ARTIFACT_NAME: links-shortener-api-v$CI_PIPELINE_IID.jar
  APP_NAME: links-shortener

stages:
  - build
  - test
  - deploy
  - post deploy
  - publish

build:
  stage: build
  image: openjdk:17-alpine
  script:
    # Check application version by changing the application.yml
    - sed -i "s/CI_PIPELINE_IID/$CI_PIPELINE_IID/" ./src/main/resources/application.yml
    - sed -i "s/CI_COMMIT_SHORT_SHA/$CI_COMMIT_SHORT_SHA/" ./src/main/resources/application.yml
    - sed -i "s/CI_COMMIT_BRANCH/$CI_COMMIT_BRANCH/" ./src/main/resources/application.yml
    - ./gradlew build
    - mv ./build/libs/go-0.1.jar ./build/libs/$ARTIFACT_NAME
  artifacts:
    paths:
      - ./build/libs/

smoke test:
  stage: test
  image: openjdk:17-alpine
  before_script:
    - apk --no-cache add curl
  script:
    - java -jar ./build/libs/$ARTIFACT_NAME &
    - sleep 30
    - curl http://localhost:8080/health | grep "UP"

code quality:
  stage: test
  image: openjdk:17-alpine
  script:
    - ./gradlew pmdMain pmdTest
  artifacts:
    when: always
    paths:
      - build/reports/pmd

unit test:
  stage: test
  image: openjdk:12-alpine
  script:
    - ./gradlew test
  artifacts:
    when: always
    paths:
      - build/reports/tests
    reports:
      junit:
        - build/test-results/test/*.xml

deploy:
  stage: deploy
  image:
    name: banst/awscli
    entrypoint: [""]
  before_script:
    - apk --no-cache add curl
    # jq is a lightweight and flexible command-line JSON processor to quickly extract data from JSON
    - apk --no-cache add jq
  script:
    - aws configure set region us-east-2
    - aws s3 cp ./build/libs/$ARTIFACT_NAME s3://$S3_BUCKET/$ARTIFACT_NAME
    - aws elasticbeanstalk create-application-version --application-name $APP_NAME --version-label $CI_PIPELINE_IID --source-bundle S3Bucket=$S3_BUCKET,S3Key=$ARTIFACT_NAME
    # use jq to parse the json
    - CNAME=$(aws elasticbeanstalk update-environment --application-name $APP_NAME --environment-name "production" --version-label=$CI_PIPELINE_IID | jq '.CNAME' --raw-output)
    - sleep 30
    - curl http://$CNAME/actuator/info | grep $CI_PIPELINE_IID
    - curl http://$CNAME/actuator/health | grep "UP"
